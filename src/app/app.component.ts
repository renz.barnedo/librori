import { Component, OnInit, ViewEncapsulation } from "@angular/core";
import { SwUpdate } from "@angular/service-worker";

import { AlertController, Platform } from "@ionic/angular";

import { SplashScreen } from "@ionic-native/splash-screen/ngx";
import { StatusBar } from "@ionic-native/status-bar/ngx";

import { Storage } from "@ionic/storage";

import { LoginService } from "./services/login.service";
import { UserService } from "./services/user.service";
import { HostListener } from "@angular/core";

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.scss"],
  encapsulation: ViewEncapsulation.None,
})
export class AppComponent implements OnInit {
  loggedIn = false;
  dark;
  _subscription;
  deferredPrompt;
  hasInstalledPwa: boolean = localStorage.hasInstalledPwa || false;

  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private storage: Storage,
    private swUpdate: SwUpdate,
    private alertController: AlertController,
    private loginService: LoginService,
    private userService: UserService
  ) {
    this.initializeApp();
    this._subscription = this.loginService.loggedInChange.subscribe((value) => {
      this.checkLoginStatus();
      document.onkeydown;
    });
  }

  async ngOnInit() {
    this.dark = (await this.storage.get("darkMode")) || false;
    this.listenForLoginEvents();
    this.checkLoginStatus();

    this.swUpdate.available.subscribe(async (res) => {
      const alert = await this.alertController.create({
        header: "Alert",
        message: "Update is available",
        buttons: ["OK"],
        backdropDismiss: false,
      });

      await alert.present();

      alert
        .onDidDismiss()
        .then(() => this.swUpdate.activateUpdate())
        .then(() => window.location.reload());
    });

    window.addEventListener("beforeinstallprompt", (e) => {
      e.preventDefault();
      this.deferredPrompt = e;
    });
    window.addEventListener("appinstalled", (evt) => {
      this.userService.presentAlertCustom(
        "App install success! Check your home screen now.",
        "Success"
      );
    });
    window.addEventListener("DOMContentLoaded", () => {
      let displayMode = "browser tab";
      const navigatorApi: any = navigator;
      if (navigatorApi.standalone) {
        displayMode = "standalone-ios";
      }
      if (window.matchMedia("(display-mode: standalone)").matches) {
        displayMode = "standalone";
      }
      if (displayMode.indexOf("standalone") > -1) {
        localStorage.hasInstalledPwa = true;
        this.hasInstalledPwa = localStorage.hasInstalledPwa;
        this.userService.presentAlertCustom(
          "App install success! Check your home screen now.",
          "Success"
        );
      }
    });
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }

  async checkLoginStatus() {
    const loggedIn = await this.storage.get("isLoggedIn");
    this.updateLoggedInStatus(loggedIn);
  }

  refreshApp() {
    window.location.reload();
  }

  updateLoggedInStatus(loggedIn: boolean) {
    setTimeout(() => {
      this.loggedIn = loggedIn;
    }, 300);
  }

  listenForLoginEvents() {
    window.addEventListener("user:login", () => {
      this.updateLoggedInStatus(true);
    });

    window.addEventListener("user:signup", () => {
      this.updateLoggedInStatus(true);
    });

    window.addEventListener("user:logout", () => {
      this.updateLoggedInStatus(false);
    });
  }

  logout() {
    if (!this.userService.checkInternetConnection()) {
      return;
    }
    this.loggedIn = false;
    this.loginService.logUserOut();
  }

  toggleDarkMode() {
    this.storage.set("darkMode", this.dark);
  }

  installPwa() {
    if (!this.deferredPrompt) {
      this.userService.presentAlertCustom(
        "App install success! Check your home screen now.",
        "Success"
      );
      return;
    }
    this.deferredPrompt.prompt();
    this.deferredPrompt.userChoice.then((choiceResult) => {
      if (choiceResult.outcome === "accepted") {
        this.userService.presentAlertCustom(
          "App install success! Check your home screen now.",
          "Success"
        );
      } else {
        this.userService.presentAlertCustom("App Install failed!", "Error");
      }
    });
  }
}
