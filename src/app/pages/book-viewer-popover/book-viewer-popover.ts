import { Component } from "@angular/core";

import { PopoverController } from "@ionic/angular";
import { ViewerService } from "../../services/viewer.service";

@Component({
  template: `
    <ion-list>
      <ion-list-header>Controls</ion-list-header>
      <ion-item button (click)="zoomIn()">
        <ion-label>
          <ion-icon name="add-circle-outline"></ion-icon> Zoom In</ion-label
        >
      </ion-item>
      <ion-item button (click)="zoomOut()">
        <ion-label
          ><ion-icon name="remove-circle-outline"></ion-icon> Zoom
          Out</ion-label
        >
      </ion-item>
    </ion-list>
  `,
})
export class BookViewerPopoverPage {
  constructor(
    public popoverCtrl: PopoverController,
    private viewerService: ViewerService
  ) {}

  zoomIn() {
    const zoomValue = parseFloat(JSON.parse(localStorage.zoom));
    if (zoomValue && zoomValue >= 2.0) {
      // toast: max zoom  already!
      return;
    }

    localStorage.zoom = (parseFloat(localStorage.zoom) + 0.01).toFixed(2);
    this.viewerService.changeZoomValue();
  }

  zoomOut() {
    const zoomValue = parseFloat(JSON.parse(localStorage.zoom));
    if (zoomValue && zoomValue < 0.5) {
      // toast: min zoom  already!
      return;
    }

    localStorage.zoom = (parseFloat(localStorage.zoom) - 0.01).toFixed(2);
    this.viewerService.changeZoomValue();
  }
}
