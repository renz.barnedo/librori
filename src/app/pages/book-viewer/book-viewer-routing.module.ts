import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { BookViewerPage } from './book-viewer.page';

const routes: Routes = [
  {
    path: '',
    component: BookViewerPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class BookViewerPageRoutingModule {}
