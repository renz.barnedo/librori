import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule } from "@angular/forms";

import { IonicModule } from "@ionic/angular";

import { BookViewerPageRoutingModule } from "./book-viewer-routing.module";

import { BookViewerPage } from "./book-viewer.page";

import { PdfViewerModule } from "ng2-pdf-viewer";
import { BookViewerPopoverPage } from "../book-viewer-popover/book-viewer-popover";

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    BookViewerPageRoutingModule,
    PdfViewerModule,
  ],
  declarations: [BookViewerPage, BookViewerPopoverPage],
  entryComponents: [BookViewerPopoverPage],
})
export class BookViewerPageModule {}
