import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { BookViewerPage } from './book-viewer.page';

describe('BookViewerPage', () => {
  let component: BookViewerPage;
  let fixture: ComponentFixture<BookViewerPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BookViewerPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(BookViewerPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
