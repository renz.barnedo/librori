import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { MenuController, Platform, PopoverController } from "@ionic/angular";
import { Storage } from "@ionic/storage";
import { UserService } from "../../services/user.service";
import { ViewerService } from "../../services/viewer.service";
import { BookViewerPopoverPage } from "../book-viewer-popover/book-viewer-popover";

@Component({
  selector: "app-book-viewer",
  templateUrl: "./book-viewer.page.html",
  styleUrls: ["./book-viewer.page.scss"],
})
export class BookViewerPage implements OnInit {
  showAll = false;
  renderText = false;
  linkTarget = "top";

  totalPages;
  isLoaded = false;

  zoom: number = localStorage.zoom || 1;
  // zoomScale = "page-height";
  // fitToPage = true;
  zoomScale = "";
  fitToPage = false;
  originalSize = false;
  autoResize = true;

  private lastOnStart: number = 0;
  private DOUBLE_CLICK_THRESHOLD: number = 500;

  nextButton: any;
  previousButton: any;

  page: number = this.getLastViewedPage();
  chosenPage: number = this.page;
  pageShouldChange: boolean = true;
  // pageShouldChange: boolean = false;

  title: string = "";

  _subscription;

  pdfBlob: ArrayBuffer;
  pdfb64;

  swiped: boolean = false;
  animation: string = "";
  innerWidth;
  bookId;

  constructor(
    private platform: Platform,
    private popoverCtrl: PopoverController,
    private viewerService: ViewerService,
    private menuController: MenuController,
    private router: Router,
    private storage: Storage,
    private userService: UserService
  ) {
    this._subscription = this.viewerService.zoomChange.subscribe((value) => {
      this.zoom = localStorage.zoom;
    });
    this.getPdf();
  }

  async getPdf() {
    try {
      const path = this.router.url;
      this.bookId = path.split("/")[3];
      const data = await this.storage.get("user");
      const book = data.eBooks.find((eBook) => eBook._id === this.bookId);
      this.pdfb64 = book.eBook;
      this.title = book.title;
      this.pdfBlob = this.base64ToArrayBuffer(this.pdfb64);
    } catch (error) {
      await this.userService.loginFirst();
    }
  }

  base64ToArrayBuffer(base64) {
    let binary_string = window.atob(base64);
    let bytes = new Uint8Array(binary_string.length);
    for (let i = 0; i < binary_string.length; i++) {
      bytes[i] = binary_string.charCodeAt(i);
    }
    return bytes.buffer;
  }

  ngOnInit() {
    localStorage.zoom = this.zoom;
    this.innerWidth = window.innerWidth;
    if (window.innerWidth >= 768) {
      this.showAll = true;
      const viewerContainer: any = document.querySelector(".viewer-container");
      const navigator: any = document.querySelector(".navigator");
      const pages: any = document.querySelector(".pages");
      viewerContainer.style.height = "auto";
      navigator.style.display = "none";
      pages.style.display = "none";
    }
  }

  ionViewWillEnter() {
    this.menuController.swipeGesture(false);
  }

  getLastViewedPage() {
    let pagesLastViewed = localStorage.lastViewedPage;
    if (!pagesLastViewed) {
      pagesLastViewed = [];
      localStorage.lastViewedPage = JSON.stringify(pagesLastViewed);
      return 1;
    }
    pagesLastViewed = JSON.parse(pagesLastViewed);
    const bookId = this.router.url.split("/")[3];
    const bookPage = pagesLastViewed.find((page) => page.bookId === bookId);
    return bookPage ? bookPage.page : 1;
  }

  setLastPageViewed() {
    const currentPage = {
      bookId: this.bookId,
      page: this.page,
    };
    let pagesLastViewed = JSON.parse(localStorage.lastViewedPage);
    const alreadyHasRecord = pagesLastViewed.find(
      (page) => page.bookId === this.bookId
    );
    if (pagesLastViewed.length && alreadyHasRecord) {
      pagesLastViewed = pagesLastViewed.map((page) => {
        if (this.bookId === page.bookId) {
          page.page = this.page;
          return page;
        }
        return page;
      });
    } else {
      pagesLastViewed = [...pagesLastViewed, currentPage];
    }
    localStorage.lastViewedPage = JSON.stringify(pagesLastViewed);
  }

  swipeRight() {
    if (window.innerWidth < 768) {
      this.previosPage();
    }
  }

  swipeLeft() {
    if (window.innerWidth < 768) {
      this.nextPage();
    }
  }

  pdfViewerOnClick(event) {
    const clickX = parseInt(
      (event.currentX / this.platform.width()).toFixed(2)
    );
    this.nextButton = document.querySelector("#next");
    this.previousButton = document.querySelector("#previous");
    if (clickX >= 0.9) {
      this.nextButton.click();
    } else if (event.currentX / this.platform.width() <= 0.1) {
      this.previousButton.click();
    } else {
      this.nextButton.click();
      const now = Date.now();
      if (Math.abs(now - this.lastOnStart) <= this.DOUBLE_CLICK_THRESHOLD) {
        this.zoomOnDoubleClick(event);
        this.lastOnStart = 0;
      } else {
        this.lastOnStart = now;
      }
    }
  }

  zoomOnDoubleClick(event) {}

  afterLoadComplete(pdfData: any) {
    this.totalPages = pdfData.numPages;
    this.isLoaded = true;
  }

  nextPage() {
    if (this.page >= this.totalPages && window.innerWidth < 768) {
      this.userService.presentToastFirstLast("last");
      return;
    }
    this.page++;
    this.setLastPageViewed();
    this.chosenPage = this.page;
  }

  previosPage() {
    if (this.page <= 1 && window.innerWidth < 768) {
      this.userService.presentToastFirstLast("first");
      return;
    }
    this.page--;
    this.setLastPageViewed();
    this.chosenPage = this.page;
  }

  changePage() {
    if (!this.chosenPage || localStorage.page) {
      this.pageShouldChange = true;
    }

    if (
      this.chosenPage > this.totalPages ||
      !this.pageShouldChange ||
      (!this.chosenPage && window.innerWidth < 768)
    ) {
      this.userService.presentAlert(
        "Less than 1 or more than total pages of the book is not allowed."
      );
      return;
    }
    this.page = this.chosenPage;
  }

  pageRendered(event) {}

  textLayerRendered(event) {}

  onError(event) {
    // this.userService.presentToastCustom(
    //   "Please have an internet connection when viewing this book for the first time only."
    // );
  }

  onProgress(event) {}

  async presentPopover(event: Event) {
    const popover = await this.popoverCtrl.create({
      component: BookViewerPopoverPage,
      event,
    });
    await popover.present();
  }
}
