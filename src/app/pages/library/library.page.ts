import { Component, OnInit } from "@angular/core";
import { Storage } from "@ionic/storage";
import { LibraryService } from "../../services/library.service";
import { UserService } from "../../services/user.service";

@Component({
  selector: "app-library",
  templateUrl: "./library.page.html",
  styleUrls: ["./library.page.scss"],
})
export class LibraryPage implements OnInit {
  books;
  showScreen: boolean = false;
  user;

  constructor(
    private storage: Storage,
    private libraryService: LibraryService,
    private userService: UserService
  ) {}

  ngOnInit() {}

  async setBooks() {
    try {
      this.user = await this.storage.get("user");
      this.books = this.user.eBooks.filter((eBook) => eBook.deviceLimit);
      this.showScreen = true;
      this.userService.closeLoading();
    } catch (error) {
      await this.userService.loginFirst();
    }
  }

  async ionViewWillEnter() {
    await this.setBooks();
  }

  async getUpdatedBooks(event) {
    await this.getEbooks();
    await this.setBooks();
    event.target.complete();
  }

  async refreshBooks() {
    await this.getEbooks();
    await this.setBooks();
  }

  async getEbooks() {
    if (!this.userService.checkInternetConnection()) {
      return;
    }
    this.userService.presentLoading();
    try {
      const { token, email, eBooks } = this.user;
      const data = {
        token,
        email,
        eBooks: eBooks.map((eBook) => ({
          _id: eBook._id,
          deviceLimit: eBook.deviceLimit,
        })),
      };
      const response: any = await this.libraryService.getEbooks(data);
      this.user = { ...this.user, eBooks: response.eBooks };
      await this.storage.set("user", this.user);
      this.userService.presentToastCustom("Updated all books");
    } catch (error) {
      if (error.error.message) {
        await this.userService.presentAlert(error.error.message);
        return;
      }
      await this.userService.loginFirst();
    }
  }
}
