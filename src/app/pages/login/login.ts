import { Component } from "@angular/core";
import { NgForm } from "@angular/forms";
import { Router } from "@angular/router";
import { UserOptions } from "../../interfaces/user-options";
import { Storage } from "@ionic/storage";
import { LoginService } from "../../services/login.service";
import { UserService } from "../../services/user.service";

@Component({
  selector: "page-login",
  templateUrl: "login.html",
  styleUrls: ["./login.scss"],
})
export class LoginPage {
  login: UserOptions = { email: "", password: "" };
  submitted = false;
  isLoading: boolean = false;
  passwordType: string = "password";
  passwordIcon: string = "eye-outline";

  constructor(
    public router: Router,
    private storage: Storage,
    private loginService: LoginService,
    private userService: UserService
  ) {}

  async ionViewWillEnter() {
    const user = await this.storage.get("user");
    if (user) {
      this.router.navigateByUrl("/app/tabs/library");
    }
  }

  togglePasswordVisibility() {
    if (this.passwordType === "password") {
      this.passwordType = "text";
      this.passwordIcon = "eye-off-outline";
      return;
    }
    this.passwordType = "password";
    this.passwordIcon = "eye-outline";
  }

  async onLogin(form: NgForm) {
    this.submitted = true;
    if (form.status === "VALID") {
      if (!this.userService.checkInternetConnection()) {
        return;
      }
      this.userService.presentLoading();
      try {
        const response: any = await this.loginService.logUserIn({
          ...this.login,
          device: this.loginService.getDeviceInfo(),
        });
        if (response.message === "no books available") {
          await this.userService.closeLoading();
          await this.userService.presentAlert(
            "Device limit reached in all of your books. Log out other device to log-in on this device."
          );
          return;
        }
        await this.storage.set("user", { ...response.data.user });
        await this.storage.set("device", response.data.devices.slice(-1));
        await this.storage.set("devices", response.data.devices);
        await this.storage.set("isLoggedIn", true);
        this.router.navigateByUrl("app/tabs/library");
        this.loginService.updateLogStatus();
        await this.userService.closeLoading();
        this.userService.presentToastSuccess();
      } catch (error) {
        await this.userService.closeLoading();
        if (error.error.message) {
          await this.userService.presentAlert(error.error.message);
          return;
        }
        await this.userService.presentAlert("Cannot login your account");
      }
      return;
    }
    await this.userService.presentAlert("Username/Password is invalid.");
  }
}
