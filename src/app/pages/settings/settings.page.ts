import { Component, OnInit } from "@angular/core";
import { Device } from "@ionic-native/device/ngx";
import { Storage } from "@ionic/storage";
import { LoginService } from "../../services/login.service";
import { SettingsService } from "../../services/settings.service";
import { UserService } from "../../services/user.service";

@Component({
  selector: "app-settings",
  templateUrl: "./settings.page.html",
  styleUrls: ["./settings.page.scss"],
})
export class SettingsPage implements OnInit {
  eBooks = [];
  devices = [];
  isMobile = window.innerWidth <= 425;
  user;

  constructor(
    private device: Device,
    private loginService: LoginService,
    private storage: Storage,
    private settingsService: SettingsService,
    private userService: UserService
  ) {}

  async ionViewWillEnter() {
    this.setDevices();
  }

  async setDevices() {
    try {
      this.devices = await this.storage.get("devices");
      this.devices = this.devices.map((device) => {
        const name = this.getDeviceInfo(device);
        return { ...device, name };
      });
      this.user = await this.storage.get("user");
      this.eBooks = this.user.eBooks.map((eBook) => ({
        title: eBook.title,
        deviceLimit: eBook.deviceLimit,
      }));
    } catch (error) {
      this.userService.loginFirst();
      this.userService.presentAlert("Device set up error.");
    }
  }

  getDeviceInfo(base = navigator) {
    const info = base.userAgent.split(")")[0].split("(")[1].split(";");
    let name = info[1];
    if (info[2]) {
      name = name + info[2];
    }
    return name;
  }

  ngOnInit() {}

  async logout() {
    if (!this.userService.checkInternetConnection()) {
      return;
    }
    await this.storage.set("isLoggedIn", false);
    await this.loginService.updateLogStatus();
    this.loginService.logUserOut();
  }

  async updateDeviceData(event) {
    await this.setNewDevices();
    await this.setDevices();
    event.target.complete();
  }

  async setNewDevices() {
    if (!this.userService.checkInternetConnection()) {
      return;
    }
    try {
      this.userService.presentLoading();
      const response: any = await this.settingsService.getDevicesDetails({
        email: this.user.email,
        token: this.user.token,
      });
      await this.storage.set("devices", response.devices);
      this.userService.presentToastCustom("Updated device limit and list");
      this.userService.closeLoading();
    } catch (error) {
      await this.userService.loginFirst();
    }
  }

  onRefreshButtonClick() {
    this.setNewDevices();
    this.setDevices();
  }
}
