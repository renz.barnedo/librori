import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { TabsPage } from "./tabs-page";

const routes: Routes = [
  {
    path: "tabs",
    component: TabsPage,
    children: [
      {
        path: "settings",
        loadChildren: () =>
          import("../../pages/settings/settings.module").then(
            (m) => m.SettingsPageModule
          ),
      },
      {
        path: "library",
        children: [
          {
            path: "",
            loadChildren: () =>
              import("../../pages/library/library.module").then(
                (m) => m.LibraryPageModule
              ),
          },
        ],
      },
      {
        path: "",
        redirectTo: "/library",
        pathMatch: "full",
      },
      {
        path: "login",
        loadChildren: () =>
          import("../../pages/login/login.module").then((m) => m.LoginModule),
      },
      { path: "**", redirectTo: "/login" },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TabsPageRoutingModule {}
