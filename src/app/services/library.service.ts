import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
// import { environment } from "../../environments/environment";
import { environment } from "../../environments/environment.prod";

@Injectable({
  providedIn: "root",
})
export class LibraryService {
  constructor(private http: HttpClient) {}

  getEbooks(data) {
    const { email, eBooks, token } = data;
    return new Promise((resolve, reject) => {
      this.http
        .post(
          `${environment.host}/product/ebooks`,
          { email, eBooks },
          {
            headers: {
              Authorization: "Bearer " + token,
            },
          }
        )
        .subscribe(
          (response) => resolve(response),
          (error) => reject(error)
        );
    });
  }
}
