import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Router } from "@angular/router";
import { Storage } from "@ionic/storage";
import { Subject } from "rxjs";
// import { environment } from "../../environments/environment";
import { environment } from "../../environments/environment.prod";
import { UserService } from "./user.service";

@Injectable({
  providedIn: "root",
})
export class LoginService {
  isAComputer = window.innerWidth > 768;
  loggedInChange: Subject<number> = new Subject<number>();

  host: string = environment.host;
  constructor(
    private http: HttpClient,
    private router: Router,
    private storage: Storage,
    private userService: UserService
  ) {}

  logUserIn(data) {
    return new Promise((resolve, reject) => {
      this.http.post(`${this.host}/user/login`, data).subscribe(
        (response) => resolve(response),
        (error) => reject(error)
      );
    });
  }

  updateLogStatus() {
    this.loggedInChange.next();
  }

  async logUserOut() {
    const user = await this.storage.get("user");
    const data = {
      email: user.email,
      devUuid: user.devUuid,
    };
    const token = (await this.storage.get("user")).token;
    localStorage.clear();
    await this.storage.clear();
    this.router.navigateByUrl("/login");
    this.userService.presentAlertCustom("Your account has been logged out.");
    return new Promise(async (resolve, reject) => {
      this.http
        .post(`${this.host}/user/logout`, data, {
          headers: {
            Authorization: "Bearer " + token,
          },
        })
        .subscribe(
          (response) => resolve(response),
          (error) => reject(error)
        );
    });
  }

  getDeviceInfo() {
    return {
      userAgent: navigator.userAgent,
    };
  }
}
