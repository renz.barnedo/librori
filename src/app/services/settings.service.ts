import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
// import { environment } from "../../environments/environment";
import { environment } from "../../environments/environment.prod";

@Injectable({
  providedIn: "root",
})
export class SettingsService {
  host: string = environment.host;
  constructor(private http: HttpClient) {}

  getDevicesDetails(data) {
    return new Promise((resolve, reject) => {
      this.http
        .post(
          `${environment.host}/limit/devices`,
          { email: data.email },
          {
            headers: {
              Authorization: "Bearer " + data.token,
            },
          }
        )
        .subscribe(
          (response) => resolve(response),
          (error) => reject(error)
        );
    });
  }
}
