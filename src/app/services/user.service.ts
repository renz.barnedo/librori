import { Injectable } from "@angular/core";
import { Router } from "@angular/router";
import {
  AlertController,
  LoadingController,
  ToastController,
} from "@ionic/angular";

@Injectable({
  providedIn: "root",
})
export class UserService {
  constructor(
    private alertController: AlertController,
    private loadingController: LoadingController,
    private router: Router,
    private toastController: ToastController
  ) {}

  async presentAlert(message) {
    const alert = await this.alertController.create({
      cssClass: "my-custom-class",
      header: "Error",
      message,
      buttons: ["OK"],
    });

    await alert.present();
  }

  async presentAlertCustom(message, header = "Alert") {
    const alert = await this.alertController.create({
      cssClass: "my-custom-class",
      header,
      message,
      buttons: ["OK"],
    });

    await alert.present();
  }

  async loginFirst() {
    this.presentAlert("Please login first.");
    this.router.navigateByUrl("login");
  }

  async presentLoading() {
    const loading = await this.loadingController.create({
      cssClass: "my-custom-class",
      message: "Please wait...",
      spinner: "crescent",
    });
    await loading.present();
  }

  async closeLoading() {
    await this.loadingController.dismiss();
  }

  async presentToastFirstLast(firstLast) {
    const toast = await this.toastController.create({
      header: "Alert",
      message: `This is the ${firstLast} page.`,
      duration: 1000,
      color: "light",
      position: "top",
    });
    toast.present();
  }

  async presentToastCustom(message) {
    const toast = await this.toastController.create({
      header: "Success",
      message,
      duration: 2000,
      color: "success",
      position: "top",
    });
    toast.present();
  }

  async presentToastSuccess() {
    const toast = await this.toastController.create({
      header: "Success",
      color: "success",
      cssClass: "success-toast",
      message: "You are logged in.",
      duration: 900,
      position: "top",
    });
    await toast.present();
  }

  checkInternetConnection() {
    const onLine = window.navigator.onLine;
    if (!onLine) {
      this.presentAlert("No internet connection.");
    }
    return onLine;
  }
}
