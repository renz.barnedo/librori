import { Injectable } from "@angular/core";
import { Subject } from "rxjs";

@Injectable({
  providedIn: "root",
})
export class ViewerService {
  zoomChange: Subject<number> = new Subject<number>();

  constructor() {}

  changeZoomValue() {
    this.zoomChange.next();
  }
}
